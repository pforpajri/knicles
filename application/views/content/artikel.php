<?php
	$c_sum = count($data_comment);
	$topik = explode(',',$data->article_topik);
	$total_topik = count($topik);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= strtoupper($data->article_judul) ?> | Knicles</title>
	<?php
		$this->load->view('header/link');
		$this->load->view('header/meta');
	?>
<style type="text/css">
	.inline {
		display: inline-block;
		padding-right: 15px;
		padding-bottom:0;
	}
	ul{
		padding: 0;
	}

	.img-article{ 
		object-fit: cover;
		height: 100% !important;	
		width: 100% !important;
		height: auto;
		border-radius: 1%;
	}
	#text{
		font-size: 18px;
		line-height: 2.0
	}
	.tag-info{
		font-size: 18px;
		display: inline-block;
		width: 100%;
	}
	.img-account-comment{
	  border-radius: 50%;
	  width: 40px;
	  height:40px;
	  object-fit: cover;
	}
	small{
		font-size: 15px;
	}
	.input-lg{
		width: 100%;
		padding: 10px 16px;
		line-height: 1.33;
		display: block;
		border: 1px solid;
		color:#555;
	}

	#img-thumbnail{
		object-fit: cover;
		width: 100%;
		max-height: 360px;
	}

	/*img{
		object-fit: cover;
		width: 100% !important;
	}*/
</style>
</head>
<body>
	<?php $this->load->view('header/navbar'); ?>
	<div id="head-part" class="container-fluid p-4 pt-4">
		<div class="row pt-4">
			<div id="blank-left" class="col-sm-1"></div> <!-- space -->
		
			<div id="content" class="col-sm-7">
				<div id="judul" class="text-left">
					<h2><b><?= strtoupper($data->article_judul) ?></b></h2>
				</div>
				<div id="article-info">
					<ul>
						<li class="inline">
						<span>
							<a><i class="fa fa-clock-o"></i> <?= $data->article_created ?></a>
						</span>
						</li>
						<li class="inline">
						<span>
							<a><i class="fa fa-user"></i> <?= strtoupper($data->nama_lengkap) ?></a>
						</span>
						</li>
						<li class="inline">
						<span>
							<a><i class="fa fa-hashtag"></i> <?= strtoupper($topik[0]) ?></a>
						</span>
						</li>
						<li class="inline">
						<span>
							<a><i class="fa fa-eye"></i> <?= $data->article_views ?></a>
						</span>
						</li>
						<li class="inline">
						<span>
							<a><i class="fa fa-comment"></i> <?= $c_sum ?> KOMENTAR</a>
						</span>
						</li>
						<li class="inline">
						<!-- <span>
							<a><i class="fa fa-book"></i> 4 MIN BACA</a>
						</span> -->
						</li>
					</ul>
				</div>
				<div id="pict" class="py-4">
					<img id="img-thumbnail" src="<?= base_url('assets/thumbnail/'.$data->article_thumbnail) ?>">
				</div>
				<div id="text">
					<?= $data->article_deskripsi ?>
				</div>
				<div id="tag" class="pt-4">
					<h4><b>Tag</b></h4>
					<ul>
						<?php
							for($i = 0; $i<$total_topik; $i++) {
						?>
							<li class="inline"><span class="badge badge-secondary tag-info"><?= strtoupper($topik[$i])  ?></span></li>
						<?php }?>
					</ul>
				</div>
				<hr class="bg-dark">
				<div id="comment" class="pb-4">
					<h4><b>Comment (<?= $c_sum ?>)</b></h4>
					<?php
					for ($i = 0; $i<$c_sum; $i++ ) {
						if ($data_comment[$i]['nama_lengkap'] == "") {
							$comment_nama = $data_comment[$i]['comment_name'];
						}
						else{
							$comment_nama = $data_comment[$i]['nama_lengkap'];
						}
					?>
						<div class="comment-section">
							<div class="img-comment float-left pr-2" style="box-sizing: border-box; display: block;">
								<img src="<?= base_url('assets/img_profile/'.$data_comment[$i]['img_profile'])?>" class="img-account-comment">
							</div>
							<div class="comment-content" style="display: table-cell;">
								<h4 class="comment-name" style="border-bottom: 2px dashed">
									<?= $comment_nama ?>, 
									<small>
										<?= $data_comment[$i]['comment_created'] ?>
										<!-- <a href="#">BALAS</a> -->
									</small>
								</h4>
								<p>	
									<?= $data_comment[$i]['comment'] ?>
								</p>
								<!--reply section -->
								<!-- <div class="reply-section">
									<div class="img-comment float-left pr-2" style="box-sizing: border-box; display: block;">
										<img src="<?= base_url('assets/img/bagus.jpg')?>" class="img-account-comment">
									</div>
									<div class="comment-content" style="display: table-cell;">
										<h4 class="comment-name" style="border-bottom: 2px dashed">
											Pajri Zahrawaani Ahmad, 
											<small>25 Juli 2021 16.24</small>
										</h4>
										<p>	
											Lorem ipsum dolor sit amet.
										</p>
									</div>
								</div> -->
							</div>
						</div>
					<?php } ?>

					<div class="create-comment pt-4">
						<h4 style="border-bottom: 2px dashed">TINGGALKAN KOMENTAR</h4>
						<?php
							if (!$this->session->userdata('username')) {
						?>
							<div class="row">
								<div class="col-sm-6 my-2">
									<input id="comment_name" type="text" style="height:46px" name="comment_name" class="input-lg" tabindex="1" placeholder="NAMA">
								</div>
								<div class="col-sm-6 my-2">
									<input id="comment_email" type="text" style="height:46px" name="comment_email" class="input-lg" tabindex="1" placeholder="EMAIL">
								</div>
							</div>
						<?php
							}
						?>
						<div class="row py-2">
							<div class="col-sm-12">
								<textarea id="comment_fill" class="input-lg" style="width: 100%;" name ="comment_fill" rows="3" placeholder="KOMENTAR" spellcheck="false"></textarea>
							</div>
						</div>
						<input id="send_comment" type="submit" class="btn btn-info pt-2" name="send_comment" value="KIRIM KOMENTAR">
					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<h4 class="text-center">MODE BACA</h4>
				<div id="kotak-sialan" class="p-2 mt-2 mb-2">
		          <i id="matahari-sialan" class="fa fa-sun-o"></i> 
		          <i id="bulan-sialan" class="fa fa-moon-o"></i>  
		        </div>
			</div>
			<div id="blank-right" class="col-sm-1"></div> <!-- space -->
		</div>
		
	</div>

	<?php if(!$this->session->userdata('username')){ $this->load->view('footer/form_modal'); }?>
	<?php $this->load->view('footer/footer') ?>
	<script type="text/javascript" src="<?= base_url('assets/js/ajax/articlePage.js') ?>"></script>
	<script type="text/javascript">
		$(function(){
			$('nav').show();
		});
		$("#send_comment").click(function(){
			createComment(<?= $data->article_id ?>)
		})
	</script>
</body>
</html>