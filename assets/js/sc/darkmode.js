function toDark(){
	$("#matahari-sialan").animate({left: '75%'});
	$("#matahari-sialan").delay(2).fadeOut();
	$("#bulan-sialan").delay(800).fadeIn();
	$("#matahari-sialan").animate({left: '0%'});
	$("#kotak-sialan").css({"background-color":"#c0c0c0","color":"black"});

	//main page
	$('body').css({"background-color":"#343a40",'color':'white'});
	$("#img-main-page").attr("src",base_url('assets/img/image-main-page-abu.png'));
	$("#k-logo").attr("src",base_url('assets/img/k-logo-nav-abu.png'));
	$("#firstAppearance").css({"background-color":"#135f66"});

	//navbar and form
	$("nav").css({"background-color":"#135f66"});
	$("#nav-title").css({"color":"#e1e1e1"});
	$('#login-form,#register-form,#verify-form,#share-form').css({'background':'#343a40'});
	$(".fa-search").css({'color':'white'});

	// all content
	$('.a-footer').css({'color':'white'})
	$('.card, .card-body').css({'background-color':'#135f66'})

}

function toLight(){
	$("#bulan-sialan").animate({left: '0%'});
	$("#bulan-sialan").delay(2).fadeOut();
	$("#matahari-sialan").delay(800).fadeIn();
	$("#bulan-sialan").animate({left: '75%'});
	$("#kotak-sialan").css({"background":"#ffc107"});

	//main page
	$('body').css({"background-color":"transparent",'color':"black"});
	$("#img-main-page").attr("src",base_url('assets/img/image-main-page.png'));
	$("#k-logo").attr("src",base_url('assets/img/k-logo-nav.png'));
	$("#firstAppearance").css({"background-color":"#e1e1e1"});

	//navbar and form
	$("nav").css({"background-color":"#e1e1e1"});
	$("#nav-title").css({"color":"#135f66"});
	$('#login-form,#register-form,#verify-form,#share-form').css({'background':'white'});
	$('.fa-search').css({'color':'black'})

	//all content
	$('.a-footer').css({'color':'black'})

}

$(function(){
	$("#bulan-sialan").hide();

	$("#matahari-sialan").click(function(){
		toDark();
		localStorage.toggled = 'dark';
	});
	$("#bulan-sialan").click(function(){
		toLight();
		localStorage.toggled = 'light';
	});

	if (localStorage.toggled == 'dark') {
		toDark()
	}
	else if (localStorage.toggled == 'light') {
		toLight()
	}

}); 