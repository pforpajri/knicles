
<footer id="rFooter" class="container-fluid">
	<div id="footer">
		<div id="upFooter" class="row">
			<div id="upFooterLeft" class="col-sm-6 pt-2">
				<div class="row pt-4">
					<div class="col-sm-3">
						<h4 id="lainnya" class="pl-4">Lainnya : </h4>
					</div>
					<div class="col-sm-6">
						<ul>
						<li><a class="a-footer" href="<?= base_url('t/img2pdf') ?>">Image to PDF Converter</a></li>
						<li><a class="a-footer" href="">Tentang Pembuat</a></li>
						<li><a class="a-footer" href="#">Berdonasi</a></li>
						<li><a class="a-footer" href="https://github.com/hendisantika/List-All-Programming-Telegram-Group">Gabung Komunitas</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div id="upFooterRight" class="col-sm-6 pt-4">
					<p>Kunjungi media sosial kami</p>
					<!-- <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fknicles&tabs&width=350&height=150&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=383279509309298" width="350" height="150" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" class="mr-2">
					</iframe> -->
			</div>
		</div>
		<div id="downFooter" class="py-3">
		Made With <i class="fa fa-heart"></i> | Knicles <?= date('Y') ?>
		</div>
	</div>	
</footer>