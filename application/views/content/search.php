<!DOCTYPE html>
<html>
<head>
	<title>Home | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
</head>
<style type="text/css">
	a {
		text-decoration: none;
	}
</style>
<body>
<?php $this->load->view('header/navbar'); ?>
	<div id="viewAr" class="container-fluid">
		<div id="rAll" class="mb-4">
			<div id="allPost" class="row">
				<div id="allPostTitle" class="col-sm-12">
					<h2 class="pt-2 pb-2">Pencarian Untuk "<?= $pattern ?>"</h2>
				</div>

				<!-- start allPostLeft -->
				<div id="allPostLeft" class="col-sm-8">
					<?php 
					if (count($dataSearch) < 1) {
						echo "Tidak ada artikel terkait";
					}
					else{
						for($i = 0; $i < count($dataSearch); $i++){ 
					?>
					<div class="row m-1 my-4" style="height: auto">
						<div class="col-sm-5 p-0">
							<p id="infoWritterAll">
								<img class="img-popular-post px-2 py-2" src="<?= base_url('assets/thumbnail/'.$dataSearch[$i]->article_thumbnail) ?>">
							</p>
						</div>
						<div class="col-sm-7 p-0">
							<p id="infoWritterAll"> <img class="img-account-popular-post" src="<?= base_url('assets/img_profile/'.$dataSearch[$i]->img_profile) ?>"></img> &nbsp;<?= $dataSearch[$i]->nama_lengkap ?> <b id="timeWrittenAll" class="float-right"><i class="fa fa-clock-o"></i> <?= $dataSearch[$i]->article_created ?></b></p>
							<h5><?= $dataSearch[$i]->article_judul ?></h5>
							<p id="sub3" style="">
							<?php
								$desc = substr(preg_replace(array('/\s{2,}/', '/[\t\n]/'), "", strip_tags($dataSearch[$i]->article_deskripsi)),0,170)."... ";
								echo $desc;
								
								$suffix_uri = substr(md5($dataSearch[$i]->article_id), 0,7);
								$t = trim($dataSearch[$i]->article_judul);
								$exp = explode(" ", $t);
								$t = implode($exp,'-');

							?>
								<a href="<?= base_url('p/'.strtolower($t).'-'.$suffix_uri) ?>">
									<span class="teksSelengkapnya">
									Baca Selengkapnya
									</span>
								</a>
							</p> 
						</div>
					</div>
				<?php } } ?>

				</div>
				<div id="allPostRight" class="col-sm-4">
					<center><h4>Mode Baca</h4></center>
					<div id="kotak-sialan" class="p-2 mt-2 mb-2">
			          <i id="matahari-sialan" class="fa fa-sun-o"></i> 
			          <i id="bulan-sialan" class="fa fa-moon-o"></i>  
			        </div>
			        <div id="tagTopic" class="pt-4 mt-4">
		        		<center>Tag Topik
		        			<p id="allTags" class="justify-content-between" style="font-size: 20px">
			        			<?php 
			        				for ($i=0; $i < count($dataTag); $i++) { 
			        				$badge = ['secondary','info','primary','warning','danger','success'];
			        				$dataTag[$i] = strtoupper($dataTag[$i]);
	        					?>
		        				<a href="<?= base_url('article/tag/').htmlspecialchars(trim($dataTag[$i])) ?>">
		        					<span id="<?= $dataTag[$i] ?>" class="badge badge-<?= $badge[array_rand($badge)] ?>"><?= $dataTag[$i] ?></span>
		        				</a>
			        			<?php } ?>
		        			</p>
		        		</center>
					</div>
					<div class="pt-4 mt-4">
						<center>
							<!-- soon -->
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('footer/form_modal') ?>
	<?php $this->load->view('footer/footer') ?>