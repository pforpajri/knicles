<?php
$sum = $data_count[0]['status_sum']+$data_count[1]['status_sum']+$data_count[2]['status_sum'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dashboard Admin | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css">
	<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
<?php $this->load->view('header/navbar'); ?>
	<div class="container">
		<div class="row py-2">
			<h2>Dashboard Admin</h2> 
		</div>
		<div class="row container-fluid mb-4">
			<?php
				$card = [ 
							['text-info','Summary',$sum,'fa fa-list float-right'],
							['text-warning', $data_count[0]['article_status'], $data_count[0]['status_sum'],'fa fa-hourglass float-right'],
							['text-success', $data_count[1]['article_status'], $data_count[1]['status_sum'],'fa fa-hourglass float-right'],
							['text-danger', $data_count[2]['article_status'], $data_count[2]['status_sum'],'fa fa-hourglass float-right'],
						];
				for ($i=0; $i < count($card); $i++) { 
			?>

			<div class="col-md-3 my-2 px-2">
				<div class="card <?= $card[$i][0]?>">
				  <h5 class="card-header" style="cursor: pointer;" onclick="tableType('<?= $card[$i][1] ?>')"><?= $card[$i][1] ?></h5>
				  <div class="card-body">
				  	<h1>
				  		<span><?= $card[$i][2] ?></span>
				    	<i class="<?= $card[$i][3] ?>" style="margin-top: 5px;"></i>
				  	</h1>
				  </div>
				</div>
			</div>
			<?php } ?>
		</div>

		<div class="row py-2">
			<?php isset($type) == true ? $dataType = $type : $dataType = "Keseluruhan"; ?>
			<h2>Data <?= $dataType ?></h2> 
		</div>
		<div class="row container-fluid my-4">
			<div class="col-sm-12">
				<table class="table table-striped table-bordered dtbl">
					<thead>
						<tr>			
							<th>Username</th>
							<th>Nama Pembuat</th>
							<th>Judul</th>
							<th>Topik</th>
							<th>Status</th>
							<th>Opsi</th>
							<th>Preview</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>

	</div>
	<?php $this->load->view('footer/footer') ?>
	<script type="text/javascript">
		let tableType = (type) =>{
			if (type == "Summary") {
				type = "";
			}
			document.location.href= '<?= base_url('admin/dashboard/') ?>'+type;
		}
		let red = (url,bool) => bool ? window.location.href = url : false


		$(function(){

			$('.dtbl').DataTable({
				"pageLength"		: 10,
          		"scrollY"           : "500px",
				"ajax"				: {
					<?php isset($type) == true ? $urlT = ['methodType' => 'dtblT/', 'uriType' => $type] : $urlT = ['methodType' => 'dtbl', 'uriType' => null] ?>
		            url : "<?= base_url("admin/".$urlT['methodType'].$urlT['uriType'].'/'. md5($this->session->userdata('role')) )?>",
		            type : 'GET'
		        },
		        "language"			: {
	                "emptyTable"  : "Data artikel tidak tersedia",
	                "zeroRecords" : "Data artikel tidak ditemukan"
	            },
			});
		});

	</script>
</body>
</html>