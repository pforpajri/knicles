<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('role') != 'admin') {
			redirect(base_url());
		}
	}

	public function index(){
		redirect(base_url('admin/dashboard'));
	}

	public function dashboard($type = null){
		$data = $this->AdminModel->countDataArticle();
		
		$status = ["Pending", "Accepted", "Rejected"];
		$data_count = [];
		$migrateDataValue = [];

		for ($i=0; $i < 3; $i++) {
			for ($j=0; $j < count($data); $j++) { 
				$data[$j]['article_status'] == $status[$i]? $migrateDataValue[$status[$i]] = $data[$j]['status_sum'] : "";
			}

			$data_count[$i] = ['article_status' => $status[$i], 'status_sum' => strval(0)];
			array_key_exists($status[$i], $migrateDataValue)? $data_count[$i]['status_sum'] = $migrateDataValue[$status[$i]] : "";
		}

		if (!$type) {
			$data = [ 'data_count' => $data_count];
			$this->load->view('admin/admin_coba',$data);
		}
		else{

			$data = [ 'data_count' => $data_count, 'type' => $type];
			$this->load->view('admin/admin_coba',$data);	
		}

	}

	public function preview($title){
		$title = str_replace('-',' ', $title);
		$aid = $this->uri->segment(4);
		$aid = substr($aid, 0,7);

		$a =  $this->AdminModel->previewData($title,$aid)->row();
		$data = ["data" => $a];
		$this->load->view('admin/preview',$data);

	}
	public function delete($article_id){
		$where = "md5(article_id) LIKE  '$article_id%' ";
		$this->AdminModel->delete_article($where);
		redirect(base_url('admin/dashboard'));
	}

	public function dtbl($md5 = null){
		if (!$md5){
			redirect(base_url('admin'));
		}
		if ($md5 === md5($this->session->role)) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	     	$length = intval($this->input->get("length"));

		    $article = $this->AdminModel->getAllDataArticle();
		    $data = [];

		    foreach ($article->result() as $ar) {
		    	$url_hapus = htmlspecialchars(base_url('admin/delete/'.md5($ar->article_id)));
		    	$ajud = strtolower(str_replace(' ', '-', $ar->article_judul));
		    	$url_preview = base_url('admin/preview/'.$ajud.'/'.md5($ar->article_id));
				
		      	$data[] = [
		      				$ar->username,
		      				$ar->nama_lengkap,
		      				$ar->article_judul,
		      				$ar->article_topik,
		      				"<form method='POST' action='".base_url('admin/changeStatus/'.$ar->article_id)."'>
		      				<select name='status' class='form-select form-control-sm' onchange='if(this.value != 0) { this.form.submit(); }'>
			      				<option value='Pending' ". ($ar->article_status == 'Pending'? 'selected':'') ." >Pending</option>
			      				<option value='Accepted' ". ($ar->article_status == 'Accepted'? 'selected':'') .">Accepted</option>
			      				<option value='Rejected' ". ($ar->article_status == 'Rejected'? 'selected':'') .">Rejected</option>
		      				</select>
		      				</form>",
		      				"<button class='mt-1 btn btn-danger' onclick='red(`".$url_hapus."`,confirm(`anda yakin?`))'> <i class='fa fa-trash-o'></i> </button>",
		      				"<button class='mt-1 btn btn-info' onclick='red(`$url_preview`,true)'> <i class='fa fa-eye'></i> </button>"
		      				];
		    }

		    $o = [
		           "draw" => $draw,
		            "recordsTotal" => $article->num_rows(),
		            "recordsFiltered" => $article->num_rows(),
		            "data" => $data
		      		];

		    echo json_encode($o);
		    exit();
		}
		else{
			redirect(base_url('admin'));
		}
	}

	public function dtblT($type){
		$md5 = $this->uri->segment(4);
		if (!$type && $md5){
			redirect(base_url('admin'));
		}
		if ($md5 === md5($this->session->role)) {
			$draw = intval($this->input->get("draw"));
	      	$start = intval($this->input->get("start"));
	     	$length = intval($this->input->get("length"));

	     	$where = ['art.article_status' => $type];
	    	$data = [];
		    $article = $this->AdminModel->getWhereDataArticle($where);
		    if (empty($article->result())) {
		    	$data[] = null;
		    }
		    else{

			    foreach ($article->result() as $ar) {
			    	$url_hapus = htmlspecialchars(base_url('admin/delete/'.md5($ar->article_id)));
			    	$ajud = strtolower(str_replace(' ', '-', $ar->article_judul));
			    	$url_preview = base_url('admin/preview/'.$ajud.'/'.md5($ar->article_id));
			      	$data[] = [
			      				$ar->username,
			      				$ar->nama_lengkap,
			      				$ar->article_judul,
			      				$ar->article_topik,
			      				"<form method='POST' action='".base_url('admin/changeStatus/'.$ar->article_id)."'>
			      				<select name='status' class='form-select form-control-sm' onchange='if(this.value != 0) { this.form.submit(); }'>
				      				<option value='Pending' ". ($ar->article_status == 'Pending'? 'selected':'') ." >Pending</option>
				      				<option value='Accepted' ". ($ar->article_status == 'Accepted'? 'selected':'') .">Accepted</option>
				      				<option value='Rejected' ". ($ar->article_status == 'Rejected'? 'selected':'') .">Rejected</option>
			      				</select>
			      				</form>",
			      				"<button class='mt-1 btn btn-danger' onclick='red(`$url_hapus`,confirm(`anda yakin?`))'> <i class='fa fa-trash-o'></i> </button>",
			      				"<button class='mt-1 btn btn-info' onclick='red(`$url_preview`,true)'> <i class='fa fa-eye'></i> </button>"
			      				];
			    }

		    }


		    $o = [
		           "draw" => $draw,
		            "recordsTotal" => $article->num_rows(),
		            "recordsFiltered" => $article->num_rows(),
		            "data" => $data
		      		];

		    echo json_encode($o);
		    exit();
		}
		else{
			redirect(base_url('admin'));
		}
	}

	public function changeStatus($ar_id){
		$status = ["article_status" => $this->input->post('status')];
		$where = ['article_id' => $ar_id];
		$this->AdminModel->changeStatus($status,$where);
		redirect('admin/dashboard');
	}

}