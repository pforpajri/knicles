<!DOCTYPE html>
<html>
<head>
	<title>Image to PDF Converter | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
</head>
<style type="text/css">
	a {
		text-decoration: none;
	}
</style>
<body>
	<?php $this->load->view('header/navbar'); ?>
	
	<div class="container">
		<div class="row py-4 px-2 pl-4">
			<h2>Image to PDF Converter</h2>
		</div>
		<div class="row container-fluid mb-4">
			<div class="col-sm-8">
				<div class="form-group">
					<h5 class="text-justify">
						Tools untuk mengubah gambar menjadi PDF tanpa registrasi maupun watermark, feel free to use.
					</h5>
				</div>
		  		<div class="form-group my-4">
	    			<label for="file">Pilih Gambar</label>
		    		<input type="file" required="on" class="form-control" id="file" placeholder="Pilih Gambar">
			  	</div>
		  		<div class="form-group my-4">
				  	<button type="submit" id="convert" class="btn btn-primary" onclick="convert()">Convert</button>
		  		</div>
				  	<span id="infoConvert"></span>
			</div>	
		</div>
	</div>
	<div class="fixed-bottom mt-4">
		<?php $this->load->view('footer/form_modal') ?>
		<?php $this->load->view('footer/footer') ?>
	</div>
	<script type="text/javascript" src="<?= base_url('assets/js/sc/darkmode.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/ajax/convert.js') ?>"></script>
	<script type="text/javascript">
		$(function(){
			$("#firstAppearance").slideDown(350);
			$("nav").slideDown(400);
			$("#header_text").show();
			$("#home_bg").fadeIn(1500);
			$("#paragraph").fadeIn(1500);
			$("#contain1").fadeIn(2000);
		});
	</script>	
</body>
</html>