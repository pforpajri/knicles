<?php

class UserModel extends CI_Model{
	protected $table = 'account';

	public function view_profile($table,$data){
		return $this->db->get_where($table,$data)->row();
	}
	public function add_username_profile($table,$data){
		$this->db->set($data);
		$this->db->insert($table);
	}

	public function editIdentity($data,$where){
		$this->db->where($where);
		$this->db->update($this->table,$data);
	}

	public function editPassword($data,$where){
		$this->db->where($where);
		$this->db->update($this->table,$data);
	}

	public function editImg($data,$where){
		$this->db->where($where);
		$this->db->update($this->table,$data);
	}

	public function getIMG($where){
		return $this->db->get_where($this->table,$where)->row();
	}

}