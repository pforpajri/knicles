<!DOCTYPE html>
<html>
<head>
	<title>Home | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
</head>
<style type="text/css">
	a {
		text-decoration: none;
	}
</style>
<body>
<?php $this->load->view('header/navbar'); ?>
	<div id="firstAppearance" class="container-fluid" style="background-color: #E1E1E1; height: 400px">
		<div id="rFirst">
			<div id="firstView" class="row">
				<div id="firstViewLeft" class="col-sm-6 pl-4">
					<h1 id="titleFirstView" class="pt-4 pb-4">Share your knowledge into articles</h1>
					<p id="textFirstView1">Bagikan hal-hal yang bermanfaat bagi kita!</p>
				    <p>Mari tingkatkan minat literasi kita :)</p>
					<button id="buttonFirstView" <?php if($this->session->userdata('username')){ echo "onclick='createButtonFV()'"; } ?> class="btn my-2 my-sm-0 ml-auto m-4" data-toggle="modal" data-target="#myModal">Buat Artikel</button>
				</div>
				<div id="firstViewRight" class="col-sm-6 pr-4">
					<center>
						<img id="img-main-page" class="mt-4 pt-4 align-bottom" src='<?= base_url('assets/img/image-main-page.png') ?>'></img>
					</center>
				</div>
		    </div>
		</div>
	</div>

	<div id="viewAr" class="container-fluid">
		<div id="rPopular">
			<div id="popularPost" class="row">
				<div id="popularPostTitle" class="col-sm-12">
					<h2 class="pt-2 pb-2">Postingan Populer</h2>
				</div>
				<?php for($i = 0; $i < count($dataPopular); $i++){ ?>
				<!-- start popular post -->
				<div id="popular-1" class="col-sm-4 pt-2 pb-4">
					<img class="img-popular-post" src="<?= base_url('assets/thumbnail/'.$dataPopular[$i]->article_thumbnail) ?>">
					<p id="infoWritter">
						<img class="img-account-popular-post" src="<?= base_url('assets/img_profile/'.$dataPopular[$i]->img_profile) ?>">  <?= $dataPopular[$i]->nama_lengkap ?>
						<b id="timeWritten"><i class="fa fa-clock-o"></i> <?= $dataPopular[$i]->article_created ?></b>
					</p>
					<h5><?= $dataPopular[$i]->article_judul ?></h5>
					<p id="sub1" style="height: 96px;">
						<?php
						$desc = substr(preg_replace(array('/\s{2,}/', '/[\t\n]/'), "", strip_tags($dataPopular[$i]->article_deskripsi)),0,170)."... ";
						echo $desc;
						$suffix_uri = substr(md5($dataPopular[$i]->article_id), 0,7);
						$t = trim($dataPopular[$i]->article_judul);
						$exp = explode(" ", $t);
						$t = implode($exp,'-');
						?>
					</p> 
					<a href="<?= base_url('p/'.strtolower($t).'-'.$suffix_uri) ?>">
						<span class="teksSelengkapnya">
						Baca Selengkapnya
						</span>
					</a>
					<div id="infoPost" style="" class="row">
						<div id="infoViewerPost" style="text-align:center" class="col-sm-6">
							<?= strval($dataPopular[$i]->article_views) ?> <i class="fa fa-eye"></i>
						</div>
						<div id="infoSharePost" style="text-align:center" class="col-sm-6">
							<button id="share" type="submit" class="btn btn-transparent pt-2" data-toggle='modal' data-target='#shareModal' name="share" onclick="tampungLink.putLink('<?= base_url('p/'.strtolower($t).'-'.$suffix_uri) ?>')">
								<i class="fa fa-share"></i>
								Bagikan
							</button>
						</div>
					</div>
				</div>
				<!-- end popular post -->

				<?php } ?>

			</div>
		</div>
		<!-- end rPopular -->
		<hr style="border: solid; background: black">
		<!-- start rAll  -->
		<div id="rAll">
			<div id="allPost" class="row">
				<div id="allPostTitle" class="col-sm-12">
					<h2 class="pt-2 pb-2">Semua Postingan</h2>
				</div>

				<!-- start allPostLeft -->
				<div id="allPostLeft" class="col-sm-8">


				<?php for($i = 0; $i < count($dataAll); $i++){ ?>
					<div class="row m-1 my-4" style="height: auto">
						<div class="col-sm-5 p-0">
							<p id="infoWritterAll">
								<img class="img-all-post px-2 py-2" src="<?= base_url('assets/thumbnail/'.$dataAll[$i]->article_thumbnail) ?>">
							</p>
						</div>
						<div class="col-sm-7 p-0">
							<p id="infoWritterAll"> <img class="img-account-popular-post" src="<?= base_url('assets/img_profile/'.$dataAll[$i]->img_profile) ?>"></img> &nbsp;<?= $dataAll[$i]->nama_lengkap ?> <b id="timeWrittenAll" class="float-right"><i class="fa fa-clock-o"></i> <?= $dataAll[$i]->article_created ?></b></p>
							<h5><?= $dataAll[$i]->article_judul ?></h5>
							<p id="sub3" style="">
							<?php
								$desc = substr(preg_replace(array('/\s{2,}/', '/[\t\n]/'), "", strip_tags($dataAll[$i]->article_deskripsi)),0,170)."... ";
								echo $desc;
								
								$suffix_uri = substr(md5($dataAll[$i]->article_id), 0,7);
								$t = trim($dataAll[$i]->article_judul);
								$exp = explode(" ", $t);
								$t = implode($exp,'-');

							?>
								<a href="<?= base_url('p/'.strtolower($t).'-'.$suffix_uri) ?>">
									<span class="teksSelengkapnya">
									Baca Selengkapnya
									</span>
								</a>
							</p> 
						</div>
					</div>
				<?php } ?>

				</div>
				<!-- end allPostLeft -->

				<div id="allPostRight" class="col-sm-4">
					<center><h4>Mode Baca</h4></center>
					<div id="kotak-sialan" class="p-2 mt-2 mb-2">
			          <i id="matahari-sialan" class="fa fa-sun-o"></i> 
			          <i id="bulan-sialan" class="fa fa-moon-o"></i>  
			        </div>
			        <div id="tagTopic" class="pt-4 mt-4">
		        		<center>Tag Topik
		        			<p id="allTags" class="justify-content-between" style="font-size: 20px">
			        			<?php 
			        				for ($i=0; $i < count($dataTag); $i++) { 
			        				$badge = ['secondary','info','primary','warning','danger','success'];
			        				$dataTag[$i] = strtoupper($dataTag[$i]);
	        					?>
		        				<a href="<?= base_url('article/tag/').htmlspecialchars(trim($dataTag[$i])) ?>">
		        					<span id="<?= $dataTag[$i] ?>" class="badge badge-<?= $badge[array_rand($badge)] ?>"><?= $dataTag[$i] ?></span>
		        				</a>
			        			<?php } ?>
		        			</p>
		        		</center>
					</div>
					<div class="pt-4 mt-4">
						<center>
							<!-- soon -->
						</center>
					</div>
				</div>
			</div>
		</div>
		<!-- end rAll-->
	</div>
	<?php $this->load->view('extras/shareModal') ?>
	<?php $this->load->view('footer/form_modal') ?>
	<?php $this->load->view('footer/footer') ?>
	<script type="text/javascript" src="<?= base_url('assets/js/sc/darkmode.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/sc/other_function.js') ?>"></script>
	<script type="text/javascript">
		$(function(){
			$("#firstAppearance").slideDown(350);
			$("nav").slideDown(400);
			$("#header_text").show();
			$("#home_bg").fadeIn(1500);
			$("#paragraph").fadeIn(1500);
			$("#contain1").fadeIn(2000);
		});
		<?php if($this->session->userdata('username')) { ?> 
			
		function createButtonFV(){ window.location.href = "<?= base_url('article/my/create') ?>"}

	 	<?php } ?>
	</script>
</body>
</html>