<!DOCTYPE html>
<html>
<head>
	<title>Buat | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
<script src="https://cdn.ckeditor.com/4.17.1/full/ckeditor.js"></script>
<style type="text/css">
	.inline {
		display: inline-block;
		padding-right: 15px;
		padding-bottom:0;
	}
	ul{
		padding: 0;
	}

	.img-article{
		/*object-fit: cover;*/
		width: 100% !important;
		height: auto;
		border-radius: 1%;
	}
	#text{
		font-size: 18px;
		line-height: 2.0
	}
	.tag-info{
		font-size: 18px;
		display: inline-block;
		width: 100%;
	}
	.img-account-comment{
	  border-radius: 50%;
	  width: 40px;
	  height:40px;
	  object-fit: cover;
	}
	small{
		font-size: 15px;
	}
	.input-lg{
		width: 100%;
		padding: 10px 16px;
		line-height: 1.33;
		display: block;
		border: 1px solid;
		color:#555;
	}
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/sc/style.css') ?>">
</head>
<body class="">
	<?php $this->load->view('header/navbar'); ?>
	<div id="head-part" class="container-fluid p-4 pt-4">
		<div class="row pt-4">
			<div id="blank-left" class="col-sm-1"></div> <!-- space -->

			<div id="contentArticle" class="col-sm-7">
				<form id="createArticle" method="POST" enctype="multipart/form-data">
					<div id="judul" class="text-left pb-4">
						<div class="md-form">	
				            <input type="text" name="article_title" class="form-control validate" required="" placeholder="Judul Artikel">
				        </div>
					</div>
					<div id="topik" class="text-left pb-4">
						<div class="md-form">	
				            <input type="text" name="article_topic" class="form-control validate" required="" placeholder="Topik/Tag Artikel">
				        </div>
					</div>
					<div id="thumbnail" class="text-left pb-4">
						<div class="form-control md-form custom-button">
				  			<input type="file" name="article_thumbnail" id="article_thumbnail" multiple="" readonly="" required="">
					  	</div>
				 	</div>
					<div id="deskripsi" class="text-left pb-4">
						<textarea name="article_description" id="textEditor" class="py-2"></textarea>
					</div>
					<div class=" text-center">
						<!-- <button id="PreviewArticle" class="btn btn-info">Preview</button>  = soon-->
						<button class="btn btn-info">Selesai</button>
						<div id="status_data" style="display: hidden"></div>
					</div>
				</form>
			</div>

			<div class="col-sm-3"></div> <!-- addotional space-->
			<div id="blank-right" class="col-sm-1"></div> <!-- space -->
		</div>
	</div>

	<?php $this->load->view('footer/form_modal') ?>
	<?php $this->load->view('footer/footer') ?>
	<script type="text/javascript">
		$(function(){$('nav').show();});
	</script>
	<!-- ckeditor -->
	<script type="text/javascript">
		CKEDITOR.replace('textEditor',{
			height : 500,
			filebrowserUploadUrl : '<?= base_url('article/uploadContentImage') ?>',
			filebrowserUploadMethod : 'form',
			disallowedContent : 'img{width}',
			on:{
				instanceReady: function(){
					this.dataProcessor.htmlFilter.addRules({
						elements:{
							img: function(el){
								if (!el.attributes.alt)
									el.attributes.alt = 'An Image';

								el.addClass('img-article');
							}
						}
					});
				}
			}
		});
	</script>

	<!-- AJAX -->
	<script type="text/javascript" src="<?= base_url('assets/js/ajax/articlePage.js') ?>"></script>
	<script type="text/javascript">
		//$("#PreviewArticle").click(function(){}); soon

		$("form#createArticle").submit(function(e){
			e.preventDefault()

			for(instance in CKEDITOR.instances){
				CKEDITOR.instances.textEditor.updateElement()
			}

			var formData = new FormData(this)
			
			let el = document.getElementById('status_data');
			createArticle(formData,'<?= base_url('article/createArticle/') ?>',el)
		});
	</script>
</body>
</html>