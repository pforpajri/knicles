<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proses extends CI_Controller {

  //tbl name
  protected $reg_table = "account", $pre_table = "pre_account";

  //email account
  protected $smtp_host = 'ssl://smtp.zoho.com', 
            $smtp_user = 'verif.knicles@zohomail.com',
            $smtp_pass = '$ThisP4ssword';

  public function __construct(){
    parent::__construct();
    $this->load->model('AuthModel');
  }

  public function pre_register(){
    $r=array(
      'nama_lengkap' => htmlspecialchars($this->input->post('nama_lengkap')),
      'email' => htmlspecialchars($this->input->post('email')),
      'username' => htmlspecialchars($this->input->post('username')),
      'password' => htmlspecialchars($this->input->post('password')),
    );

    
    if ($r['nama_lengkap'] != null AND $r['email'] != null AND $r['username'] != null AND $r['password'] != null) {

      $data = [
                    'pre_nama_lengkap' => $r['nama_lengkap'],
                    'pre_email' => $r['email'],
                    'pre_username' => $r['username'],
                    'pre_password' => md5($r['password']),
                    'verify_token' => md5(uniqid(rand()))
                  ];

      $p =$this->AuthModel->view_pre_account($this->pre_table,['pre_email' => $r['email']]);
      $p2 =$this->AuthModel->get_account($this->reg_table,['email' => $r['email']]);
      $usernameUnregistered = $this->AuthModel->get_account($this->pre_table,['pre_username' => $r['username']]);
      $usernameRegistered = $this->AuthModel->get_account($this->reg_table,['username' => $r['username']]);

      if ($p == 1) {
        $label = "<label data-error='wrong' data-success='right' for='verify-token' style='font-size: 14px;'><i class='fa fa-exclamation-triangle'>
                    </i>&nbsp; Email {$r['email']} Sudah terdaftar namun belum verifikasi. Cek email anda dan masukkan kode verifikasinya atau
                  </label>";
        echo json_encode(array('verify' => "{$label}"));
      }
      else if($p2 == 1){
        echo json_encode(array('verified' => '<span class="badge badge-danger">Email telah terdaftar !</span>'));
      }
      else if ($usernameRegistered === 1 OR $usernameUnregistered === 1) {
        echo json_encode(array('verified' => '<span class="badge badge-danger">Username telah dipakai!</span>'));
      }
      else{
        $kode   = $data['verify_token'];
        $pesan = "Hai {$r['nama_lengkap']}, terima kasih telah mendaftar :). Anda akan menjadi salah satu dari kami.<br> Berikut kode verifikasi anda : $kode";
        $config = [
             'mailtype'  => 'html',
             'charset'   => 'iso-8859-1',
             'protocol'  => 'smtp',
             'smtp_host' => $this->smtp_host,
             'smtp_user' => $this->smtp_user,
             'smtp_pass' => $this->smtp_pass,     
             'smtp_port' => 465,
             'crlf'      => "\r\n",
             'newline'   => "\r\n"
         ];
 
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($this->smtp_user, 'no-reply');
        $this->email->to($r['email']);
        $this->email->subject('Verifikasi Email Knicles');
        $this->email->message($pesan);

        if (@$this->email->send()) {
          $this->AuthModel->auth_register($this->pre_table,$data);
          echo json_encode(array('verify' => '<span class="badge badge-info"><i class="fa fa-info-circle"></i>&nbsp; Kode telah terkirim ke email anda</span>'));
        } else {
          echo json_encode(array('error' => '<span class="badge badge-danger"> Error! email verifikasi tidak dapat dikirim. masukan email yang valid! </span><br>' ,'err_status' => $this->email->print_debugger()));
        }
      } 
    }
    else{
      echo json_encode(array('error' => '<span class="badge badge-danger">Isi form pendaftarannya !</span>'));
    }
  }

  public function resend_email(){
    $data = ['pre_email' => htmlspecialchars($this->input->post("email"))];
    if ($data['pre_email'] != null) {
      $data['verify_token']= md5(uniqid(rand()));

      $kode = $data['verify_token'];
      $pesan = "Hai, terima kasih telah mendaftar :). Anda akan menjadi salah satu dari kami.<br> Berikut kode verifikasi anda : $kode";
      $config = [
             'mailtype'  => 'html',
             'charset'   => 'iso-8859-1',
             'protocol'  => 'smtp',
             'smtp_host' => $this->smtp_host,
             'smtp_user' => $this->smtp_user,
             'smtp_pass' => $this->smtp_pass,     
             'smtp_port' => 465,
             'crlf'      => "\r\n",
             'newline'   => "\r\n"
         ];

      $this->email->initialize($config);
      $this->email->set_newline("\r\n");
      $this->email->from($this->smtp_user, 'no-reply');
      $this->email->to($data['pre_email']);
      $this->email->subject('Verifikasi Ulang Email Knicles');
      $this->email->message($pesan);

      if (@$this->email->send()) {
          $this->AuthModel->edit_pre_account($this->pre_table,$data,$where=['pre_email' => $data['pre_email']]);
          echo json_encode(array("check_verify" => "&nbsp;<i class='fa fa-check'></i>", 'data' => $data));
      } else {
          echo json_encode(array('error' => '<span class="badge badge-danger"> Error! email verifikasi tidak dapat dikirim. masukan email yang valid! </span><br>' ,'err_status' => $this->email->print_debugger()));
      }
    }
    else{
      echo json_encode(array("error" => "Email Kosong !", 'data' => $data));
    }
  }

  public function register(){
    $verify_token = htmlspecialchars($this->input->post("verify_token"));
    $where = ['verify_token' => $verify_token];
    $get_data =$this->AuthModel->get_pre_account($this->pre_table,$where);
    if ($get_data) {
      $data = [
                'username' => $this->input->post("username"),
                'email' => $get_data->pre_email,
                'password' => md5($this->input->post("password")),
                'nama_lengkap' => $this->input->post("nama_lengkap")
              ];  
      $this->AuthModel->add_account($this->reg_table,$data);
      $this->AuthModel->delete_pre_account($this->pre_table,$where);
      echo json_encode(array('info_verified' => '<span class="badge badge-success">Berhasil mendaftar!</span>'));
    }
    elseif ($verify_token == null) {
      echo json_encode(array('error-verified' => "<span class='badge badge-warning' style='color:brown'><i class='fa fa-exclamation-triangle'></i> Isi Kode Verifikasi!</span>"));  
    }
    else{
      echo json_encode(array('error-verified' => "<span class='badge badge-danger'><i class='fa fa-exclamation-triangle'></i> Kode Salah</span>"));    
    }
  }

  public function login(){
    $data = [
              'username' => htmlspecialchars($this->input->post("username")),
              'password' => md5(htmlspecialchars($this->input->post("password")))
            ];
      if ($data['username'] != null AND $data['password'] != null) {
        $find = $this->AuthModel->get_account($this->reg_table,$data);
        if ($find) {
          $viewAccData= $this->AuthModel->view_account($this->reg_table,$data);
          $session_data = [
                            "username" => $data['username'],
                            "password" => $data['password'],
                            "nama_lengkap" => $viewAccData->nama_lengkap,
                            "email" => $viewAccData->email,
                            "role" => $viewAccData->role
                          ];

          $sess = $this->session->set_userdata($session_data);
          echo json_encode(array("aa" => $this->session->userdata()));
        }
        else{
          echo json_encode(array('login_failed' => '<span class="badge badge-danger">Username/Password salah!</span>'));
        }
      }
      else{
        echo json_encode(array('login_failed' => '<span class="badge badge-danger">Isi Username dan Password!</span>'));
      }
  }

  public function viewData(){ // debugging
    echo $this->session->userdata('username');
  }

}