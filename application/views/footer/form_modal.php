<?php
$session['username'] = $this->session->userdata('username');
$session['password'] = $this->session->userdata('password');
$session['email'] = $this->session->userdata('email');
if (!$session['username'] AND !$session['password'] AND !$session['email']) {
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">

    <div id="login-form" class="modal-content">
      <div class="modal-header text-center">
        <h4 id="modal_title" class="modal-title w-100 font-weight-bold">Silahkan Masuk</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body mx-3">
          <div class="md-form mb-4">
            <input type="text" id="username-login" class="form-control validate" required="">
            <label data-error="wrong" data-success="right" for="username"><p style="color: #ced4da; float: left;"><b>╰</b></p>Username</label>
          </div>

          <div class="md-form mb-4">
            <input type="password" id="password-login" class="form-control validate" required="">
            <label data-error="wrong" data-success="right" for="password"><p style="color: #ced4da; float: left;"><b>╰</b></p>Password</label>
          </div>

          <div class="md-form" >
            <p id="info-login" style="display: none;"></p>
            Tidak punya akun? 
            <span>
              <a id="daftar" style="cursor: pointer; color: #2B7279">Daftar</a>
            </span>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button id="submit_login" class="btn " style="background-color: #2B7279; color:white;">Masuk</button>
        </div>
    </div>

    <div id="register-form" class="modal-content">
      <div class="modal-header text-center">
        <h4 id="modal_title" class="modal-title w-100 font-weight-bold">Daftar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body mx-3">
          <div class="md-form mb-4">
            <input type="text" id="nama_lengkap-daftar" class="form-control validate">
            <label data-error="wrong" data-success="right" for="nama_lengkap"><p style="color: #ced4da; float: left;"><b>╰</b></p>Nama Lengkap</label>

            <input type="email" id="email-daftar" class="form-control validate">
            <label data-error="wrong" data-success="right" for="email"><p style="color: #ced4da; float: left;"><b>╰</b></p>Email</label>

            <input type="text" id="username-daftar" class="form-control validate">
            <label data-error="wrong" data-success="right" for="username"><p style="color: #ced4da; float: left;"><b>╰</b></p>Username</label>

            <input type="password" id="password-daftar" class="form-control validate">
            <label data-error="wrong" data-success="right" for="password"><p style="color: #ced4da; float: left;"><b>╰</b></p>Password</label>

          </div>
          <div class="md-form" >
            <p id="info-register" style="display: none;"></p>
              Sudah punya akun? 
              <span>
                <a id="masuk" style="cursor: pointer; color: #2B7279">Masuk</a>
              </span>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-center fa-3x">
          <button id="submit_daftar" class="btn " style="background-color: #2B7279; color:white;">Daftar</button>
        </div>
    </div>

    <div id="verify-form" class="modal-content">
      <div class="modal-header text-center">
        <h4 id="modal_title" class="modal-title w-100 font-weight-bold">Verifikasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div id="verify-modal" class="modal-body mx-3">
          <div class="md-form mb-4">
            <label data-error="wrong" data-success="right" for="verify-token">Masukkann kode verifikasi!</label>
            <input type="text" id="verify-token" class="form-control validate">
            <div id="info-verify" class="py-2" for="verify-token"></div>
            <button id="submit_resend" class="btn btn-dark" style="color:white; display: none">Kirim ulang kodenya</button>
          </div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button id="submit_verify" class="btn " style="background-color: #2B7279; color:white;">Verifikasi</button>
        </div>
    </div>
    
  </div>
</div>

<script type="text/javascript">

  // form viewer
  $(function(){
    $("#register-form").hide();
    $("#verify-form").hide();
    $('#daftar').click(function(){
        $('#login-form').slideUp(750);
        setInterval(1000);
        $('#register-form').delay(750).slideDown(750);
        document.getElementById("submit_daftar").innerHTML = "Daftar";
    });
    $('#masuk').click(function(){
        document.getElementById("info-login").innerHTML = "";
        $('#register-form').slideUp(750);   
        setInterval(1000);
        $('#login-form').delay(750).slideDown(750);
    });
    $('#login_button').click(function(){
        document.getElementById("info-login").innerHTML = "";
        $("#register-form").hide();
        $("#verify-form").hide();
        $('#login-form').fadeIn(1000);
    });
    $('#buttonFirstView').click(function(){
        document.getElementById("info-login").innerHTML = "";
        $("#register-form").hide();
        $("#verify-form").hide();
        $('#login-form').fadeIn(1000);   
    });
  });

  if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
  }

  //submit Daftar
  $(function(){
    $('#submit_daftar').click(function(){
      if (document.getElementById("verify-form").style.opacityscript == 0) {
        document.getElementById("submit_daftar").innerHTML += " <i class='fa fa-spinner fa-spin'></i>";
      }
      document.getElementById("submit_resend").innerHTML = "Kirim ulang kodenya";
      
      $.post('<?= base_url('proses/pre_register') ?>',{
        nama_lengkap: $("#nama_lengkap-daftar").val(),
        email: $("#email-daftar").val(),
        username: $("#username-daftar").val(),
        password: $("#password-daftar").val(),
      },
      function(data,status){
        var jsonData = JSON.parse(data);
        var err = jsonData['error'];
        var formModal = document.getElementById('register-form');
        var info =document.getElementById("info-verify");
        var tag = document.getElementById("info-register");

        if(err){
          tag.innerHTML = err; 
          $("#info-register").fadeIn(250);
        }
        else if(jsonData['verified']){
           sub = document.getElementById("submit_daftar").innerHTML = "Daftar";
           tag.style.display="block";
           tag.innerHTML = jsonData['verified'];
        }
        else if (jsonData['error']) {

           tag.style.display="block";
           tag.innerHTML = jsonData['error'];
        }
        else{
          $("#register-form").slideUp(750);
          $('#verify-form').delay(750).slideDown(750);
          info.innerHTML = jsonData['verify'];
          $("#submit_resend").attr("style","color:white;");

          $("#submit_resend").click(function(){
            $.post("<?= base_url('proses/resend_email') ?>",
            {
             email: $("#email-daftar").val()
            },
            function(data,status){
              jsonDataResend = JSON.parse(data);
              console.log(jsonDataResend);
              if (jsonDataResend['check_verify']) {
                document.getElementById("submit_resend").innerHTML = "Kirim ulang kodenya " + jsonDataResend['check_verify'];
                $("#submit_resend").css({"background-color":"green"});
              }
              else if (jsonDataResend['error']) {
                 $("#submit_resend").attr('disabled','');
                 $("#submit_resend").html(jsonDataResend['error']);
                $("#submit_resend").css({"background-color":"red"});
              }

            });
          });
        }
        $("#daftar").click(function(){
          jsonData['error'] == "";
          document.getElementById("info-register").style.display ="none";
          document.getElementById("info-register").innerHTML ="";
        });
      });
    });
  });


  //submit login
  $(function(){
    $("#submit_login").click(function(){
      $.post("<?= base_url("proses/login") ?>",
      {
        username:$("#username-login").val(),
        password:$("#password-login").val()
      },
      function(data,status){
        jsonDataLog = JSON.parse(data);
        if (jsonDataLog['login_failed']) {
          document.getElementById("info-login").setAttribute("style","");
          document.getElementById("info-login").innerHTML = jsonDataLog['login_failed'];
        }
        else{
          // alert(data);
          $.post("<?= base_url("proses/viewData/")?>",{
              data: jsonDataLog['username']
          },
          function(data,status){
            location.reload();
          });
        }
      });
    });
  });


  //for verify
  $(function(){
    $("#submit_verify").click(function(){
      $.post("<?= base_url("proses/register")?>",
      {
        verify_token:$("#verify-token").val(),
        username:$("#username-daftar").val(),
        nama_lengkap:$("#nama_lengkap-daftar").val(),
        password:$("#password-daftar").val()
      },
      function(data,status){
          jsonDataVer = JSON.parse(data);
          if (jsonDataVer['info_verified']) {
            $("#register-form").hide();
            $("#verify-form").slideUp(750);
            $('#login-form').delay(750).slideDown(750);
            $("#info-login").css({"display":"block"});
            document.getElementById("info-login").innerHTML = jsonDataVer['info_verified'];
          }
          else{
            document.getElementById("info-verify").innerHTML = jsonDataVer['error-verified'];
          }
      });
    });
  });
</script>
<?php } ?>