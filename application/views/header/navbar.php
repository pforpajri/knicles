<?php
$session['username'] = $this->session->userdata('username');
$session['password'] = $this->session->userdata('password');
$session['email'] = $this->session->userdata('email');

?>
<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #E1E1E1;">
  <a class="navbar-brand" href="<?= base_url() ?>" draggable="false"><img id="k-logo"  style="width: 50px; height: 50px;" src="<?= base_url('assets/img/k-logo-nav.png') ?>"><b id="nav-title" style="color: #2B7279">Knicles</b></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
     <div class="navbar-form navbar-right hidden-xs py-2" role="search">

        <div class="input-group" id="search-component">
          <input class="form-control" id="input_cari" placeholder="Cari Artikel" type="text" style="border-radius: 5px;">
            <span class="input-group-btn">
                <button type="submit" id="submit_cari" class="btn btn-default">
                  <i class="fa fa-search"></i>
                </button>
            </span>
        </div>

    </div>
         
      <div id='profile' class='my-2 my-sm-0 ml-auto mr-2'>
    <?php
      if (!$session['username'] OR !$session['password'] OR !$session['email']) {
      	$pojok_kanan = "	<button id='login_button' class='btn my-2 my-sm-0 ml-auto mr-2' style='color: #2B7279; border-color: #2B7279;'  data-toggle='modal' data-target='#myModal' >Gabung</button>";
      }
      else{
        $ingfo = $this->UserModel->view_profile('account', ['username' => $session['username']]);
        $img_profile = $ingfo->img_profile;
        $img = base_url('assets/img_profile/'.$img_profile);
        $path = [
                  'profile' => 'profile/',
                  'my_article' => 'article/my/',
                  'keluar' => 'profile/keluar',
                  'admin' => 'admin/dashboard'
                ]; 
        if ($ingfo->role == 'admin') {
          $pojok_kanan_admin = '<button class="dropdown-item" type="button" onclick="re(`'.base_url($path['admin']).'`)">Dashboard Admin </button>';
        }
        else{
          $pojok_kanan_admin = "";
        }

        $pojok_kanan = '
                          <div id="profile-info-mobile">
                            <button class="dropdown-item" type="button" onclick="re(`'.base_url($path['profile']).'`)"> Profil</button>
                            <button class="dropdown-item" type="button" onclick="re(`'.base_url($path['my_article']).'`)">Artikel saya</button>
                            <button class="dropdown-item" type="button" onclick="re(`'.base_url($path['keluar']).'`)">Keluar </button>
                            '.$pojok_kanan_admin.'
                          </div>
                          <div class="btn-group dropleft" id="dropdown-profile">
                            <div class="" id="toggle-profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <img id="img_navbar" src="'.$img.'" id="account-img" class="rounded-circle" width="50px" height="50px" style="object-fit:cover; cursor:pointer">
                            </div>
                            <div class="dropdown-menu">
                              <button class="dropdown-item" type="button" onclick="re(`'.base_url($path['profile']).'`)"> Profil</button>
                              <button class="dropdown-item" type="button" onclick="re(`'.base_url($path['my_article']).'`)">Artikel saya</button>
                              <button class="dropdown-item" type="button" onclick="re(`'.base_url($path['keluar']).'`)">Keluar </button>
                              '.$pojok_kanan_admin.'
                            </div>
                          </div>
                        ';
      }
      echo $pojok_kanan;
    ?>
      </div>
  </div>
</nav>

<script type="text/javascript">
  var re = (path) =>{
    if (path == "<?= base_url('profile/keluar') ?>") {
      var a = confirm("Anda yakin?");
      if( a == true){
        window.location.href= path; 
      }
    }
    else{
      window.location.href= path; 
    }
  }
</script>
<script type="text/javascript">
  $(function(){
    $("#dropdown-item").hide();
    $("#account-img").click(function(){
      $(document.getElementById("dropdown-item")).animate({width: 'toggle'},200);
    });
    $("#toggle-profile").click(function(){
      $(".dropdown-menu").attr('class','dropdown-menu show');
      $(".dropdown-menu").attr('style','position: absolute; transform: translate3d(-160px, 0px, 0px); top: 0px; left: 0px; will-change: transform;');
    })
    $("#dropdown-profile").mouseleave(function(){
      $(".dropdown-menu").fadeOut();
    });
  });
</script>
<script type="text/javascript">
  $(function(){
    $("#submit_cari").css({"background-color":"transparent"});
    $("#submit_cari").click(function(){
        value = $("#input_cari").val();
        document.location.href = "<?= base_url('article/search')?>?q="+value;
    });

  });

 
</script>
