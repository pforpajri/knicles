<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller{

	protected $article_table = 'article';
	protected $allowedFormat = ['jpg','JPG','png','PNG','svg','jpeg'];

	public function __construct(){
		parent::__construct();
		if (!$this->uri->segment(1) == 'p') {
			if (!$this->session->userdata('username')) {
				redirect(base_url());
			}
		}
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index(){
		redirect(base_url());
	}

	public function search(){
		$pattern = htmlspecialchars($this->input->get('q'));
		if (!$pattern) {
			redirect(base_url());
		}


		$a = $this->ArticleModel->articleSearch($pattern)->result();
		$data = ['pattern' => $pattern, 'dataSearch' => $a];
		$data['dataTag'] = $this->allTag();
		$this->load->view("content/search",$data);
	}

	public function my($sub = null){
		if (!$sub) {
			if ($_GET) {
				if ($_GET['status'] == 1) {
					echo  '<script>alert("Artikel berhasil dibuat") </script>';
				}
				if ($_GET['status'] == 'success' AND $_GET['act'] == 'del') {
					echo  '<script>alert("Artikel berhasil dihapus") </script>';
				}
			}

			$wheree = ['username' => $this->session->userdata('username')];
			$data = $this->ArticleModel->group_article_status($wheree)->result_array();
			$status = ["Pending", "Accepted", "Rejected"];
			$data_count = [];
			$migrateDataValue = [];

			for ($i=0; $i < 3; $i++) {
				for ($j=0; $j < count($data); $j++) { 
					$data[$j]['article_status'] == $status[$i]? $migrateDataValue[$status[$i]] = $data[$j]['status_sum'] : "";
				}

				$data_count[$i] = ['article_status' => $status[$i], 'status_sum' => strval(0)];
				array_key_exists($status[$i], $migrateDataValue)? $data_count[$i]['status_sum'] = $migrateDataValue[$status[$i]] : "";
			}

			$data_countt = ['data' => $data_count];
			$this->load->view('article_set/dashboardArticle',$data_countt);
		}
		else{
			if ($sub === 'create') {
				$this->load->view('article_set/createArticle');
			}
			else if ($sub === 'del') {
				$where = ['username' => htmlspecialchars($_GET['u']), 'article_id' =>htmlspecialchars($_GET['i']) ];
				$this->ArticleModel->delete_article($where);

				header('Location:'.base_url('article/my?status=success&act=del'));
			}
			else{
				header('Location:'.base_url());
			}
		}
	}

	public function dtbl(){
      	$draw = intval($this->input->get("draw"));
      	$start = intval($this->input->get("start"));
     	$length = intval($this->input->get("length"));

 	 	$where = ['acc.username' => $this->session->userdata('username')];
	    $article = $this->ArticleModel->article_join_user($where,'not');
	    $data = [];
	    foreach ($article->result() as $ar) {
	    	$url_hapus = htmlspecialchars(base_url('article/my/del?i='.$ar->article_id.'&u='.$ar->username));

	      	$data[] = [
	      				$ar->article_judul,
	      				$ar->article_topik,
	      				substr(preg_replace(array('/\s{2,}/', '/[\t\n]/'), "", strip_tags($ar->article_deskripsi)),0,120)."...",
	      				$ar->article_created,
	      				$ar->article_status,
	      				"<button class='mt-1 btn btn-danger' onclick='red(`".$url_hapus."`,confirm(`anda yakin?`))'> <i class='fa fa-trash-o'></i> </button>"
	      				];
	    }

	    $o = [
	           "draw" => $draw,
	            "recordsTotal" => $article->num_rows(),
	            "recordsFiltered" => $article->num_rows(),
	            "data" => $data
	      		];

	    echo json_encode($o);
	    exit();

	}

	public function uploadContentImage(){
		if (isset($_FILES['upload']['tmp_name'])) {
			$file = $_FILES['upload']['tmp_name'];
			$filename = $_FILES['upload']['name'];

			$arr = explode('.', $filename);
			$format = end($arr);

			$rand = date('YmdHis');
			$newName = $rand.'.'.$format;

			if (in_array($format, $this->allowedFormat)) {
				$dst = './assets/img_content/';
				move_uploaded_file($file, $dst.$newName);
				$functionNumber = $_GET['CKEditorFuncNum'];

				$url = base_url().'assets/img_content/'.$newName;
				$message = "";

				echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction({$functionNumber},'{$url}','{$message}') </script>";
			}	
		}
	}

	public function tag($pattern = null){
		if (!$pattern) {
			redirect(base_url());
		}
		$a = $this->ArticleModel->tagSearch($pattern)->result();
		$data = ['pattern' => $pattern, 'dataSearch' => $a];

		$data['dataTag'] = $this->allTag();
		$this->load->view("content/search",$data);


	}

	private function allTag(){
		$dataArticle = $this->ArticleModel->getAllTag()->result_array();

		$newTagData = [];
		$dataTag = [];
		$k = 0;
		for ($i=0; $i < count($dataArticle); $i++) { 
			$newTagData[$i] = explode(",",$dataArticle[$i]['article_topik']);
			for ($j=0; $j < count($newTagData[$i]); $j++) { 
				$dataTag[$k] = trim(strtolower($newTagData[$i][$j]));
				$k++;
			}
		}
		$uniq = array_unique($dataTag);
		$newUniq = array_values($uniq);
		return $newUniq;
	}

	public function detail($title){
		if (!$title) {
			redirect(base_url());
		}
		if ($this->uri->segment(1) == 'article' AND $this->uri->segment(2) == 'detail') {
			redirect(base_url("p/$title"));
		}
		
		$exp = explode("-", $title);
		$suffix_uri = end($exp);
		
		$pop = array_pop($exp);
		$imp1 = implode($exp, " ");
		// $exp = explode(" ", $t);
		// $t = implode($exp,'-');

		$data_article = $this->ArticleModel->getDetailArticle($imp1,$suffix_uri);

		if ($data_article) {
			$where_article = ['ar.article_id' => $data_article->article_id];
			$data_comment = $this->ArticleModel->article_join_comment_join_account($where_article);

			$tu = strtolower(str_replace(" ", "-", $data_article->article_judul));

			$this->viewCount($data_article->article_id,$data_article->article_views);
			$this->load->view('content/artikel',['data' => $data_article,'data_comment' => $data_comment]);
		}
		else{		
			$this->load->view('errir_gan');
		}
	}

	private function viewCount($article_id,$article_views){
		$article_views += 1;
		$data = ['article_views' => $article_views];
		$where = [
					'article_id' => $article_id,
					'article_status' => 'Accepted',
				 ];
		$this->ArticleModel->addViewCount($where,$data);
	}

	//insert article
	public function createArticle(){

		if ($_FILES['article_thumbnail']['error'] != 0) {
			echo json_encode(['err' => '<span class="badge badge-danger">Masukkan thumbnail dengan lengkap!</span>']);
		}
		else{

			$filename = $_FILES['article_thumbnail']['name'];
			$f_tmp = $_FILES['article_thumbnail']['tmp_name'];
			$exp = explode('.', $filename);
			$format = strtolower(end($exp));

			$data = [
				'username' 			=> $this->session->userdata('username'),
				'article_judul'		=> $this->input->post('article_title'),
				'article_topik'		=> $this->input->post('article_topic'),
				'article_deskripsi'	=> $this->input->post('article_description')
			];
			$data['article_judul'] = strtoupper(trim($data['article_judul']));

			if (in_array($format, $this->allowedFormat)) {
				$name = strtolower(str_replace(" ", "_", $data['article_judul']));
				$dst_file = './assets/thumbnail/'.
				$img_name = $name.'.'.$format;

				if (file_exists($_SERVER['DOCUMENT_ROOT'].'/assets/thumbnail/'.$img_name)) {
					unlink($_SERVER['DOCUMENT_ROOT'].'/assets/thumbnail/'.$img_name);
				}

				move_uploaded_file($f_tmp, $dst_file);
			}

			setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
			$date = strftime( "%A, %d %B %Y", time());

			$data['article_thumbnail'] = $img_name;
			$data['article_created'] = $date;

			$this->ArticleModel->insertArticle($this->article_table,$data);

			echo json_encode(['redirect' => base_url('article/my?status=1') ]);
		}
	}

	public function addComment(){
		setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
		$date = strftime( "%A, %d %B %Y", time());

		$data = [
					"article_id" => htmlspecialchars($this->input->post('article_id')),
					"comment" => htmlspecialchars($this->input->post('comment')),
					"comment_created" => $date
				];
		if (!$this->input->post('comment_nama') AND !$this->input->post("comment_email")) {
			$data['username'] = $this->session->userdata('username');
		}
		else{
			$data['comment_name'] = htmlspecialchars($this->input->post('comment_nama'));
			$data['comment_email'] = htmlspecialchars($this->input->post('comment_email'));
			$data['username'] = "_anonymous";
		}

		$this->ArticleModel->addComment($data);

		echo json_encode($data);
	}

}

