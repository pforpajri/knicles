<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	protected $ac_table = "account";

	public function __construct(){
		parent:: __construct();
		if (! $this->session->userdata('username') AND !$this->session->userdata('password')) {
			redirect(base_url());
		}
		$this->load->library('form_validation');
		$this->load->helper('url','form');

	}

	public function index(){
		$sess = [
				'username' => $this->session->userdata('username')
				];
		$ingfo = $this->UserModel->view_profile($this->ac_table, $sess);

		$this->load->view('content/profil',$ingfo);
	}

	public function keluar(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function editIdentity(){

		$get_data = [
						'nama_lengkap' => htmlspecialchars($this->input->post('nama_lengkap')),
						'address' => htmlspecialchars($this->input->post('alamat')),
						'gender' => htmlspecialchars($this->input->post('jk')),
						'job'=> htmlspecialchars($this->input->post('pekerjaan')),
						'hobby' => htmlspecialchars($this->input->post('hobi')),
						'birth' => htmlspecialchars($this->input->post('ttl'))
					];
		$where = ['username' => htmlspecialchars($this->input->post('username'))];
		
		$this->session->set_userdata(['nama_lengkap' => $get_data['nama_lengkap']]);		
		$t =$this->UserModel->editIdentity($get_data,$where);

        echo json_encode(array('edit_success' => '<span class="badge badge-success mb-2">Data berhasil diubah</span>'));
	}

	public function editPassword(){
		$get_data = ['password' => md5($this->input->post('password'))];
		$where = [ 'username' => $this->session->userdata('username') ];

		$this->session->set_userdata($get_data);
		$e = $this->UserModel->editPassword($get_data,$where);

		echo json_encode(array('editP_success' => '<span class="badge badge-success mb-2">Password berhasil diubah</span> '));
	}

	public function editImage(){

		date_default_timezone_set('Asia/Jakarta');
		$time = date('dmY_His-');

		$config['upload_path']          = 'assets/img_profile/';
		$config['allowed_types']        = 'gif|jpg|png|svg';
		$config['max_size']             = 2000;
		$config['overwrite']			= true;
		$config['file_name']			= $time.$this->session->userdata('username');
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);
		
		$where = ['username' => $this->session->userdata('username')];
		$getImg = $this->UserModel->getIMG($where);

		if (!$this->upload->do_upload('img_data')) {
			echo json_encode(['err' => 'Masukkan gambar', 'typeInfo' => 'badge badge-danger']);
		}
		else{
			$file = $_FILES['img_data']['name'];
			$explode = explode('.', $file);
			$format = end($explode);

			$dir = base_url('assets/img_profile/');
			$filename = $config['file_name'].'.'.$format;

			// delete current image
			if ($getImg->img_profile != 'default_profile.svg') {
				if (file_exists($_SERVER['DOCUMENT_ROOT'].'/assets/img_profile/'.$getImg->img_profile)) {
					unlink($_SERVER['DOCUMENT_ROOT'].'/assets/img_profile/'.$getImg->img_profile);
				}
			}

			$data = [ 'img_profile' => $filename ];
			$where = ['username' => $this->session->userdata('username')];
			$this->UserModel->editImg($data,$where);

			echo json_encode(['info' => 'Gambar berhasil diubah', 'typeInfo' => 'badge badge-success', 'imgData' => $dir.$filename ]);
		}

	}


}