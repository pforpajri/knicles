var base_url = (uri = null,port = null) => {
	url = location.protocol
	url += "//"
	url += location.host

	port != null? url += ":"+port.toString()+"/" : url += "/"
	uri != null? url += uri : url += "/"

	return url;
}

var tampungLink = {
	theLink : "",

	putLink : function(link){
		this.theLink = link;
	},

	getLink :  function (){
		return this.theLink;
	},

	redirectLink : function(sosmedLink){
		window.open(sosmedLink + this.theLink, '_blank');
	}
}