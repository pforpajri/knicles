<!DOCTYPE html>
<html>
<head>
	<title>Profil | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/sc/style.css') ?>">
	<style type="text/css">
		.custom-button  input[type=file]::file-selector-button{
			margin-bottom: 1rem;
			padding: 8px 12px;
		}
	</style>
</head>
<body>
<?php $this->load->view('header/navbar'); ?>
	<div class="container-fluid">
		<div class="row">
			<div id="img_p" class="col-sm-4">
				<div class="card my-4">
				  <img id="imgProfile" class="card-img-top" src="<?= base_url('/assets/img_profile/'.$img_profile) ?>" alt="Card image cap">
				  <div class="card-body">
				  	<div class="custom-button" >
			  			<input type="file" name="image_profile" id="image" multiple="" required>
			  			<button id="editImage" class="btn btn-primary">Ganti</button>
			  			<div id="imgInfo" class="pt-1" style="display: hidden"></div>
				  	</div>
				  </div>
				</div>
			</div>
			<div id="info_p" class="col-sm-8 ">
				<div class="card my-4">
				  <div class="card-body">
				    <h4 class="card-title">Identitas Diri</h4>
					<div class="md-form">
						<input type="text" id="username-p" value="<?= $username ?>" class="form-control validate" readonly="">
						<label data-error="wrong" data-success="right" for="username"><p style="color: #ced4da; float: left;"><b>╰</b></p>Username</label>

						<input type="text" id="nama_lengkap-p" value="<?= $nama_lengkap ?>" class="form-control validate" required="">
						<label data-error="wrong" data-success="right" for="nama_lengkap-p"><p style="color: #ced4da; float: left;"><b>╰</b></p>Nama Lengkap</label>

						<input type="text" id="alamat-p" value="<?= $address ?>" class="form-control validate" required="">
						<label data-error="wrong" data-success="right" for="alamat-p"><p style="color: #ced4da; float: left;"><b>╰</b></p>Alamat</label><br>

						<input type="radio" class="jk" name="jk" <?php echo ($gender == "L")?  "checked" : ""; ?> value="L" > Laki-Laki
						<input type="radio" class="jk" name="jk" <?php echo ($gender == "P")?  "checked" : ""; ?> value="P"> Perempuan<br>
						<label data-error="wrong" data-success="right" for="jk"><p style="color: #ced4da; float: left;"><b>╰</b></p>Jenis Kelamin</label>

						<input type="text" id="job-p" class="form-control validate" value="<?= $job ?>" required="">
						<label data-error="wrong" data-success="right" for="job-p"><p style="color: #ced4da; float: left;"><b>╰</b></p>Pekerjaan</label>

						<input type="text" id="hobby-p" class="form-control validate" value="<?= $hobby ?>" required="">
						<label data-error="wrong" data-success="right" for="hobby-p"><p style="color: #ced4da; float: left;"><b>╰</b></p>Hobby (pisahkan dengan spasi)</label>
						
						<input type="text" id="ttl-p" class="form-control validate" value="<?= $birth ?>" required="">
						<label data-error="wrong" data-success="right" for="ttl-p"><p style="color: #ced4da; float: left;"><b>╰</b></p>Tempat/tanggal lahir</label>
					</div>

					<div id="editIdentity_info" class="mb-2"></div>

				    <button id="editIdentity" class="btn btn-primary">Simpan Perubahan</button>
				  </div>
				</div>
				<div class="card my-4">
					<div class="card-body">
						<h4 class="card-title">Ubah Password</h4>
						<div class="md-form">
							<input type="password" id="password-p" class="form-control validate" required="">
							<label data-error="wrong" data-success="right" for="password-p"><p style="color: #ced4da; float: left;"><b>╰</b></p>Password</label>
						</div>

						<div id="editPassword_info" class="mb-2"></div>

					    <button id="editPassword" class="btn btn-primary">Simpan Perubahan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer id="rFooter" class="container-fluid">
	</footer>
	<?php $this->load->view('footer/form_modal') ?>
	<script type="text/babel" src="<?= base_url('assets/js/contentWithReact/main.jsx') ?>"></script>
	<script type="text/babel">
		footer();
	</script>
	<script type="text/javascript">
		$(function(){
			$('nav').show();
		});
	</script>
	<script type="text/javascript">
		// init define variable
		//--------------

		//edit profile

		$('#editIdentity').click(function(){
			$.post("<?= base_url('profile/editIdentity') ?>",{

				username:$('#username-p').val(),
				nama_lengkap: $("#nama_lengkap-p").val(),
				alamat: $("#alamat-p").val(),
				jk : $('.jk:checked').val(),
				pekerjaan: $("#job-p").val(),
				hobi: $("#hobby-p").val(),
				ttl: $("#ttl-p").val()

			},function(data){
				jsonData = JSON.parse(data);
				$("#editIdentity_info").html(jsonData['edit_success']).hide()
				$("#editIdentity_info").fadeIn(500)
			});
		});


		//edit password

		$('#editPassword').click(function(){
			$.post("<?= base_url('profile/editPassword') ?>",{
				password : $('#password-p').val()
			}, function(data){
				jsonData = JSON.parse(data);
				$("#editPassword_info").html(jsonData['editP_success']).hide()
				$("#editPassword_info").fadeIn(500)
			});
		});

		// edit image

		$("#editImage").click(function(){
			var img_data = $('#image').prop('files')[0]

			var form_data = new FormData();
			form_data.append('img_data',img_data);

			$.ajax({
				url 		: '<?= base_url('profile/editImage') ?>',
				cache		: false,
				contentType	: false,
				processData	: false,
				data 		: form_data,
				type 		: 'POST',
				success 	: function(data){
					let jsonData = JSON.parse(data);

					let info = document.getElementById('imgInfo');
					let textInfo = "";

					if (!jsonData.err) {
						textInfo = "<badge class=' "+jsonData.typeInfo+" '>"+jsonData.info+"</badge>";
						let imgProfile = document.getElementById('imgProfile')
						let imgNavbar = document.getElementById('img_navbar')
						imgProfile.setAttribute('src',jsonData.imgData);
						imgNavbar.setAttribute('src',jsonData.imgData);
					}
					else{
						textInfo = "<badge class=' "+jsonData.typeInfo+" '>"+jsonData.err+"</badge>";
					}

					info.innerHTML = textInfo;
					$('#imgInfo').fadeIn().delay(3000).fadeOut();

					//console.log(jsonData);
				}

			});

		});
	</script>
</body>
</html>