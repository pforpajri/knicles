<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	protected $ac_table = 'account';

	public function index(){

		$where = ['art.article_status' => 'Accepted'];
		$dataAll = $this->ArticleModel->get_data_homepage($where,$orderby = "art.article_id desc");

		$popularLimit = 3;
		$dataPopular = $this->ArticleModel->get_data_homepage($where,$orderby = "art.article_views desc",$popularLimit);

		$e = ['dataAll' => $dataAll, 'dataPopular' => $dataPopular];
		$e['dataTag'] = $this->tag();
		$this->load->view('content/home',$e);
	}

	private function tag(){
		$dataArticle = $this->ArticleModel->getAllTag()->result_array();

		$newTagData = [];
		$dataTag = [];
		$k = 0;
		for ($i=0; $i < count($dataArticle); $i++) { 
			$newTagData[$i] = explode(",",$dataArticle[$i]['article_topik']);
			for ($j=0; $j < count($newTagData[$i]); $j++) { 
				$dataTag[$k] = trim(strtolower($newTagData[$i][$j]));
				$k++;
			}
		}
		$uniq = array_unique($dataTag, SORT_REGULAR);
		$newUniq = array_values($uniq);
		return $newUniq;
	}

}