function toDark(){
	$(function(){
		$("#light-mode").hide();	
		$("html, body").css({"background-color":"#555555", "color": "white"});
		$("canvas,content").show().css({"background-color":"#555555", "color":"white"});
		$("#dark-mode").hide();
		$("#light-mode").fadeIn('fast');
	});
}

function toLight(){
	$(function(){
		$("html, body").show().fadeIn().css({"background-color":"white", "color":"black"});
		$("canvas,content").show().css({"background-color":"white","color":"black"});
		$("#light-mode").hide();
		$("#dark-mode").fadeIn('fast');
	});
}
function autoDarkLight(){
	jam=new Date().getHours();
	menit=new Date().getMinutes();
	if (jam <= 7 || jam >=18) {
		toDark();
	}
	else if(jam > 7 || jam <18){
		toLight();
		if (jam > 7 && menit == 59) {
			setInterval(toLight(),60000);
		}
	}
}
function toggleDarkLight(){
	$(function(){
		$("#dark-mode").click(function(){
			toDark();
		});
		$("#light-mode").click(function(){
			toLight();
		});
	});
}
toggleDarkLight();
autoDarkLight();
