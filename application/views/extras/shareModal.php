<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <div id="share-form" class="modal-content">
      <div class="modal-header text-center">
        <h4 id="modal_title" class="modal-title w-100 font-weight-bold">Bagikan Postingan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3 d-flex justify-content-center">
          <div class="md-form mb-4">
              <button id="shareFacebook" class="btn"  onclick="tampungLink.redirectLink('http://www.facebook.com/sharer.php?u=')" style="background-color: #2B7279; color:white;"><i class="fa fa-facebook"></i> Facebook</button>
            
            <button id="shareTwitter" class="btn " style="background-color: #2B7279; color:white;" onclick="tampungLink.redirectLink('http://twitter.com/share?url=<?= base_url() ?>&text=')"><i class="fa fa-twitter"></i> Twitter</button>
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
</script>