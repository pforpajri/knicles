<?php
// var_dump($data);
$summary = $data[0]['status_sum'] + $data[1]['status_sum'] + $data[2]['status_sum'] ;
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dashboard | Knicles</title>
	<meta charset="utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		$this->load->view('header/link');
	?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css">
	<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
<?php $this->load->view('header/navbar'); ?>
	<div class="container">
		<div class="row py-2">
			<h2>Dashboard <button class="mx-2 btn btn-info" onclick="red('<?= base_url('article/my/create') ?>',true)"> <i class="fa fa-plus"></i> Add Article</button></h2> 
		</div>
		<div class="row container-fluid mb-4">
			<?php
				$card = [ 
							['text-info','Summary',$summary,'fa fa-list float-right'],
							['text-warning',$data[0]['article_status'],$data[0]['status_sum'],'fa fa-hourglass float-right'],
							['text-success',$data[1]['article_status'],$data[1]['status_sum'],'fa fa-check float-right'],
							['text-danger',$data[2]['article_status'],$data[2]['status_sum'], 'fa fa-ban float-right']
						];
				for ($i=0; $i < count($card); $i++) { 
			?>

			<div class="col-md-3 my-2 px-2">
				<div class="card <?= $card[$i][0]?>">
				  <h5 class="card-header"><?= $card[$i][1] ?></h5>
				  <div class="card-body">
				  	<h1>
				  		<span><?= $card[$i][2] ?></span>
				    	<i class="<?= $card[$i][3] ?>" style="margin-top: 5px;"></i>
				  	</h1>
				  </div>
				</div>
			</div>

			<?php } ?>
		</div>

		<div class="row container-fluid my-4">
			<div class="col-sm-12">
				<table class="table table-striped table-bordered dtbl">
					<thead>
						<tr>			
							<th>Judul</th>
							<th>Topik</th>
							<th>Deskripsi</th>
							<th>Tanggal dibuat</th>
							<th>Status</th>
							<th>Opsi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>

	<?php $this->load->view('footer/footer') ?>
	<script type="text/javascript">
		
		let red = (url,bool) => bool ? window.location.href = url : false

		$(function(){
			$('.dtbl').DataTable({
				"pageLength"		: 5,
          		"scrollY"           : "500px",
				"ajax"				: {
		            url : "<?php echo site_url("article/dtbl") ?>",
		            type : 'GET'
		        },
		        "language"			: {
	                "emptyTable"  : "Data artikel tidak tersedia",
	                "zeroRecords" : "Data artikel tidak ditemukan"
	            },
			});
		});

	</script>
</body>
</html>