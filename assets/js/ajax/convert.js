let convert = () =>{
	if ($("#file").val() == "" ) {
		$("#infoConvert").fadeIn()
			.html("Masukkan Gambar !")
			.attr('class','badge badge-warning')
			.delay(1000)
			.fadeOut();
	}
	else{

	    var form_data = new FormData();
		var file_data = $('#file').prop('files')[0]

	    form_data.append('file',file_data);
		$.ajax({
	        url			: base_url('api',2390),
	        cache		: false,
	        contentType	: false,
	        processData	: false,
	        data 		: form_data,
	        type 		: 'POST',
	        success 	: function(data){
	            let jsonData = JSON.parse(data);
	            window.open(jsonData['ok'],'_blank');
	        }
	    })
	}
}