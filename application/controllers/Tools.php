<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$method = $this->uri->segment(2);
		if (method_exists($this, $method) AND $this->uri->segment(1) == 'tools') {
			redirect(base_url('t/'.$method));
		}
	}

	public function index(){
		$this->load->view('tools/index');
	}

	public function img2pdf(){
		$this->load->view('tools/img2pdf');
	}
}

