<?php

class AuthModel extends CI_Model{

	public function auth_register($table,$data){

		$this->db->set($data);
		$this->db->insert($table);
	}

	public function view_pre_account($table,$data){

		return $this->db->get_where($table,$data)->num_rows();
	}

	public function get_pre_account($table,$data){

        return $this->db->get_where($table,$data)->row();
	}

	public function edit_pre_account($table,$data,$where){

		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function delete_pre_account($table,$where){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function add_account($table,$data){
		$this->db->set($data);
		$this->db->insert($table);
	}

	public function get_account($table,$where){
		return $this->db->get_where($table,$where)->num_rows();
	}

	public function view_account($table,$where){
		return $this->db->get_where($table,$where)->row();
	}

}

?>