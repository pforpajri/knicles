<?php

class AdminModel extends CI_Model{
	protected $tbl_article = "article", $tbl_account = "account", $tbl_comment = "comment";

	public function getAllDataArticle(){
		$this->db->select();
		$this->db->from("{$this->tbl_article} art");
		$this->db->join("{$this->tbl_account} acc", "acc.username = art.username",'left');
		return $this->db->get();
	}
	public function getWhereDataArticle($where){
		$this->db->select("*");
		$this->db->from("{$this->tbl_article} art");
		$this->db->join("{$this->tbl_account} acc", "acc.username = art.username",'left');
		$this->db->where($where);
		return $this->db->get();
	}

	public function countDataArticle(){
		$this->db->select('article_status, COUNT(article_status) as status_sum');
		$this->db->from($this->tbl_article);
		$this->db->group_by('article_status');
		return $this->db->get()->result_array();

	}

	public function previewData($where,$like){
		return $this->db->query("SELECT* FROM article art LEFT JOIN account acc ON art.username = acc.username WHERE art.article_judul = '$where' AND md5(article_id) LIKE '$like%' ");
	}
	public function delete_article($where){
		return $this->db->query("DELETE FROM article WHERE $where");
	}
	public function changeStatus($data,$where){
		$this->db->where($where);
		$this->db->update('article',$data);
	}
}
