<?php

class ArticleModel extends CI_Model{
	protected $tbl_article = "article", $tbl_account = "account", $tbl_comment = "comment";


	public function insertArticle($table,$data){
		$this->db->set($data);
		$this->db->insert($table);

	}

	public function article_join_user($where,$row_or_not){
		$this->db->select("art.*,acc.nama_lengkap,acc.email");
		$this->db->from("{$this->tbl_article} art");
		$this->db->join("{$this->tbl_account} acc", "acc.username = art.username",'left');
		$this->db->where($where);

		if ($row_or_not == 'row') {
			return $this->db->get()->row();
		}
		elseif ($row_or_not ===  'not'){
			return $this->db->get();
		}
	}

	public function delete_article($where){
		$this->db->where($where);
		$this->db->delete($this->tbl_article);
	}

	public function group_article_status($where){
		$this->db->select('article_status, COUNT(article_status) as status_sum');
		$this->db->from($this->tbl_article);
		$this->db->group_by('article_status');
		$this->db->where($where);
		return $this->db->get();
	}

	public function article_join_comment_join_account($where){
		$this->db->select("c.*,ac.nama_lengkap,ac.img_profile");
		$this->db->from("{$this->tbl_comment} c");
		$this->db->join("{$this->tbl_article} ar",'c.article_id = ar.article_id','left');
		$this->db->join("{$this->tbl_account} ac",'ac.username = c.username','left');
		$this->db->where($where);

		return $this->db->get()->result_array();


		/* SQL nya
		SELECT c.*, ac.nama_lengkap, ac.img_profile FROM comment c LEFT OUTER JOIN article ar ON ar.article_id = c.article_id LEFT OUTER JOIN account ac ON ac.username = c.username WHERE c.article_id = 4;
		*/

	}

	public function getDetailArticle($judul,$id){
		return $this->db->query("SELECT acc.nama_lengkap, acc.email, art.* FROM article as art LEFT OUTER JOIN account as acc ON art.username = acc.username WHERE article_status = 'Accepted' AND art.article_judul = '$judul' AND md5(art.article_id) LIKE '$id%' ")->row();
	}

	public function addComment($data){
		$this->db->set($data);
		$this->db->insert($this->tbl_comment);
	}

	public function get_data_homepage($where,$orderby,$limit = null){
		$this->db->select("acc.nama_lengkap, acc.img_profile, art.article_id, art.article_judul, art.article_thumbnail, art.article_deskripsi, art.article_views, art.article_created");
		$this->db->from("{$this->tbl_article} art");
		$this->db->join("{$this->tbl_account} acc","acc.username = art.username","left");

		$this->db->where($where);
		$this->db->order_by($orderby);

		if ($limit != null) {
			$this->db->limit($limit,0);
		}

		return $this->db->get()->result();

		/* SQL nya
		SELECT acc.nama_lengkap, art.article_judul, art.article_thumbnail, art.article_deskripsi, art.article_created FROM article as art LEFT OUTER JOIN account as acc ON art.username = acc.username WHERE article_status = 'Accepted' 
		*/

	}

	public function articleSearch($where){
		return $this->db->query("SELECT art.*,acc.img_profile,acc.nama_lengkap,acc.email FROM article art LEFT JOIN account acc ON acc.username = art.username WHERE art.article_judul LIKE '%$where%' AND art.article_status='accepted' ");
	}

	public function tagSearch($where){
		return $this->db->query("SELECT art.*,acc.img_profile,acc.nama_lengkap,acc.email FROM article art LEFT JOIN account acc ON acc.username = art.username WHERE art.article_topik LIKE '%$where%' AND art.article_status='accepted' ");
	}

	public function getAllTag(){
		return $this->db->query("SELECT article_topik FROM article ");
	}

	public function addViewCount($where,$data){
		$this->db->where($where);
		$this->db->update($this->tbl_article,$data);
	}

}