<?php
$desc = substr(preg_replace(array('/\s{2,}/', '/[\t\n]/'), "", strip_tags($data->article_deskripsi)),0,147)."...";
?>

<meta charset="utf-8" >
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- basic meta tag -->
<meta name="keywords" content="<?= $data->article_topik ?>">
<meta name="description" content="<?= $desc ?>">
<meta name="subject" content="<?= strtoupper($data->article_judul) ?>">
<meta name="copyright" name="knicles">
<meta name="language" content="ID">
<meta name="robot" content="index, follow">
<meta name='author' content="<?= $data->nama_lengkap ?>">
<meta name="url" content="<?= base_url($_SERVER['REQUEST_URI']) ?>">
<meta name="identifier-URL" content="<?= base_url($_SERVER['REQUEST_URI']) ?>">


<!-- OpenGraph meta tag--> 
<meta property="locale" content="en_US">
<meta property="og:type" content="article">
<meta property="og:site_name" content="Knicles">
<meta property="article:published_time" content="<?= $data->article_created ?>">
<meta property="og:url" content="<?= base_url($_SERVER['REQUEST_URI']) ?>">
<meta property="og:title" content="<?= strtoupper($data->article_judul) ?>">
<meta property="og:description" content="<?= substr(strip_tags($data->article_deskripsi), 0,149)."..." ?>">
<meta property="og:image" content="<?= base_url('/assets/thumbnail/'.$data->article_thumbnail)?>">
<!-- <meta property="og:image:width" content="745">
<meta property="og:image:height" content="360"> -->

<!-- twitter meta tag -->
<meta name="twitter:card" content="summary_large_image">
<!-- <meta name="twitter:site" content="@knicles">
<meta name="twitter:creator" content="@knicles"> -->
<meta name="twitter:title" content="<?= strtoupper($data->article_judul) ?>">
<meta name="twitter:description" content="<?= $desc ?>">
<meta name="twitter:image" content="<?= base_url('assets/thumbnail/'.$data->article_thumbnail) ?>">
<meta name="theme-color" content="#00ff00">

