
let createArticle = (data,dst,el) =>{
	$.ajax({
		url: dst,
		type: 'POST',
		data: data,
		cache: false,
		contentType: false,
		processData: false,
		success: function (cback) {
			var c = JSON.parse(cback)
		    if (c['err']) {
		    	el.innerHTML = c['err']
		    	$(el).fadeIn().delay(3000).fadeOut()
		    }
		    else{
		    	document.getElementById('createArticle').reset()
		    	window.location = c['redirect']
		    }
		}
	});

}

let createComment = (id) =>{
	let data = {
		comment : $("#comment_fill").val(),
		article_id : id
	}
	if ($("#comment_name").length && $("#comment_email").length) {
		data.comment_nama = $("#comment_name").val()
		data.comment_email = $("#comment_email").val()
	}

	$.post(base_url('article/addComment'),data,function(cback){
		let c = JSON.parse(cback)
		window.top.location = window.top.location
	})
}